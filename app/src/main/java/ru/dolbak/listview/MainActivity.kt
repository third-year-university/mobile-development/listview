package ru.dolbak.listview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ListView
import android.widget.SimpleAdapter

class MainActivity : AppCompatActivity() {
    private lateinit var listView: ListView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        listView = findViewById(R.id.listView)

        val contacts: Array<Contact> = arrayOf(
            Contact("ИИИИИИИИгорь", "88005553535"),
            Contact("Вася", "35678"),
            Contact("Галя Жрать", "9089786756453467"),
        )

        val adapter: SimpleAdapter
        listView.adapter = MyAdapter(contacts)
    }
}